class User < ActiveRecord::Base
  before_save :ensure_authentication_token
  # Include default devise modules. Others available are:
  # , :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable ,
         :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable, :registerable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me, :authentication_token

  def ensure_authentication_token!
     reset_authentication_token! if authentication_token.blank?
  end
end

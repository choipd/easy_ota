class VersionsController < ApplicationController

  before_filter :authenticate_user!, :except => [:index, :show, :wrapper, :plist]
  
  # GET /versions
  # GET /versions.json
  def index
    @app = App.find(params[:app_id])
    @versions = Version.find(:all, :conditions => ["app_id = ?", params[:app_id]], :order => "created_at DESC")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @versions }
    end
  end

  # GET /versions/1
  # GET /versions/1.json
  def show
    @app = App.find(params[:app_id])
    @version = Version.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @version }
    end
  end

  # GET /versions/new
  # GET /versions/new.json
  def new
    @app = App.find(params[:app_id])
    @version = Version.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @version }
    end
  end

  # GET /versions/1/edit
  def edit
    @app = App.find(params[:app_id])
    @version = Version.find(params[:id])
  end

  # POST /versions
  # POST /versions.json
  def create
    @app = App.find(params[:app_id])
    
    file = params[:version][:binary_file]    
    params[:version][:filename] = file.original_filename
    params[:version].delete :binary_file
    @version = Version.new(params[:version])
    puts "** version insert"    
    respond_to do |format|
      if @version.save
  
        # file save
        tmp = file.tempfile
        dir = Rails.root.join("public", "data", @app.id.to_s(), @version.id.to_s()).to_s()
        puts "DIR = " + dir
        FileUtils.mkdir_p(dir)
        file_path = File.join(dir, file.original_filename)
        puts "file_path = " + file_path
        FileUtils.cp tmp.path, file_path        
        
        # shorten url
        Bitly.use_api_version_3
        bitly = Bitly.new('edbear', 'R_188b8934fd463d8603c25899e11944c8')
        url = wrapper_app_version_url(@app, @version, :only_path => false)
        @version.shortened_url = bitly.shorten(url).short_url
        @version.save
                
        format.html { redirect_to [@app,@version], notice: 'Version was successfully created.' }
        format.json { render json: @version, status: :created, location: @version }
      else
        format.html { render action: "new" }
        format.json { render json: @version.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /versions/1
  # PUT /versions/1.json
  def update
    @app = App.find(params[:app_id])
    file = params[:version][:binary_file]    
    params[:version][:filename] = file.original_filename if file
    params[:version].delete :binary_file
    @version = Version.find(params[:id])

    respond_to do |format|
      if @version.update_attributes(params[:version])
        
        if file
          # file save
          tmp = file.tempfile
          dir = Rails.root.join("public", "data", @app.id.to_s(), @version.id.to_s()).to_s()
          file_path = File.join(dir, file.original_filename)
          puts "file_path = " + file_path
          FileUtils.rm_rf dir
          FileUtils.mkdir_p(dir)
          FileUtils.cp tmp.path, file_path
        end
        
        format.html { redirect_to app_versions_url(@app), notice: 'Version was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @version.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apps/1/versions/1
  # DELETE /apps/1/versions/1.json
  def destroy
    @version = Version.find(params[:id])    
    if params[:app_id]
      @app = App.find(params[:app_id])
    else
      @app = @version.app
    end
    
    dir = Rails.root.join("public", "data", @app.id.to_s(), @version.id.to_s()).to_s()
    puts "DIR = " + dir
    FileUtils.rm_rf dir
    
    @version.destroy

    respond_to do |format|
      format.html { redirect_to app_versions_url(@app) }
      format.json { head :ok }
    end
  end
  
  
  
  # generate html plist
  def wrapper
    @app = App.find(params[:app_id])
    @version = Version.find(params[:id])
    
    respond_to do |format|
      format.html # wrapper.html.erb
    end    
  end
  
  def plist
    @app = App.find(params[:app_id])
    @version = Version.find(params[:id])
    
    respond_to do |format|
      format.html {render :layout => false}
    end
  end
  
end

class AddAuthToken < ActiveRecord::Migration
  def change
    add_column :users, :token_authenticatable, :string
    add_column :users, :authentication_token, :string
    add_index :users, :authentication_token, :unique => true
  end
end

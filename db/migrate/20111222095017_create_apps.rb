class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string :appid
      t.string :title
      t.integer :point

      t.timestamps
    end
  end
end

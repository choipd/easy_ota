class AddShortenedUrlToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :shortened_url, :string
  end
end

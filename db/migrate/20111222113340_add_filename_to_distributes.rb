class AddFilenameToDistributes < ActiveRecord::Migration
  def change
    add_column :distributes, :filename, :string
  end
end

class CreateVersions < ActiveRecord::Migration
  def change
    create_table :versions do |t|
      t.references :app
      t.integer :revision
      t.text :description

      t.timestamps
    end
    add_index :versions, :app_id
  end
end

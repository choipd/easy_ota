class AddFilenameToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :filename, :string
  end
end
